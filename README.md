# docker-hands-on

Based on: https://github.com/docker/labs/blob/master/beginner/chapters/webapps.md

# commands

Build image:

	docker build -t <user>/catgifs:1.0 .

Start container:

	docker run -d -p 8888:5000 --name catgifs <user>/catgifs:1.0

Show active containers:

	docker ps

Stop container:

	docker stop <container_id>

Show all containers:

	docker ps -a
	
Start Docker Swarm:

	docker swarm init
	
Show Docker Swarm nodes:

	docker node ls
	
Start service:

	docker service create -p 8888:5000 --name catgifs <user>/catgifs:1.0
	
Show active services:

	docker service ls
	
Scale up service (e.g. two instances):

	docker service scale catgifs=2
