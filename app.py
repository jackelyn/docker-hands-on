from flask import Flask, render_template
import random

app = Flask(__name__)

# list of cat images
images = [
   "https://img.buzzfeed.com/buzzfeed-static/static/2017-08/9/17/asset/buzzfeed-prod-web-13/anigif_sub-buzz-11282-1502314385-1.gif",
   "https://img.buzzfeed.com/buzzfeed-static/static/2017-08/9/19/asset/buzzfeed-prod-web-05/anigif_sub-buzz-2678-1502320480-1.gif",
   "https://img.buzzfeed.com/buzzfeed-static/static/2017-08/9/19/asset/buzzfeed-prod-web-12/anigif_sub-buzz-22162-1502320833-1.gif",
   "https://img.buzzfeed.com/buzzfeed-static/static/2017-08/9/17/asset/buzzfeed-prod-web-01/anigif_sub-buzz-20568-1502314071-7.gif",
   "https://img.buzzfeed.com/buzzfeed-static/static/2017-08/9/21/asset/buzzfeed-prod-web-13/anigif_sub-buzz-20472-1502327789-1.gif"
]

@app.route('/')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")